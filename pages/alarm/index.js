const app = getApp();

Page({
  data: {
    pageIndex: 1, // 当前第几页
    pageSize: 10, // 一页展示几条
    navList: ['未处理', '已处理', '全部'],
    nav_type: 0,
    list: [],
    checked_all:false,
    trolleyArr:[],  //购物车商品id
    loading: false, // 是否展示 “正在加载” 字样
    loaded: false // 是否展示 “已加载全部” 字样
  },
  onShow: function (options) {
    var that = this
    that.getAlarmRecordList()
  },
  getAlarmRecordList() {
    var that = this
    var DealFlagTypeId = ''
    switch(that.data.nav_type){
      case 0: 
        DealFlagTypeId = 1;
        break;
      case 1: 
        DealFlagTypeId = 2;
        break;
      default:
        DealFlagTypeId = ''
        break;
    }
    var params = {
      "Offset": (that.data.pageIndex - 1) * that.data.pageSize,
      "RowCount": that.data.pageSize,
      "DealFlagTypeId": DealFlagTypeId,
      "OrderBy": 'AlarmTime',
      "isDesc": true,
    }
    app.fetch.apis('api/Report/GetAlarmRecordList', params,app.globalData.token).then(
      res => {
        if (res.State === 'success') {
          if(res.Data.length === 0 || res.Data.length > that.data.pageSize) {
            that.data.pageIndex--
            that.setData({
              loading: false, //加载中
              loaded: true //是否加载完所有数据
            });
          } else {
            that.setData({
              list: that.data.list.concat(res.Data)
            })
            that.setData({
              loading: false, //加载中
              loaded: false //是否加载完所有数据
            });
          }
          wx.hideNavigationBarLoading()
          wx.stopPullDownRefresh()
        } else {
          wx.showModal({
            showCancel: false,
            content: res.Message
          });
        }
      }
    )
  },
  changeType: function (e) {
    let {
      index
    } = e.currentTarget.dataset;
    if (index === undefined) {
      return false;
    } else {
      this.setData({
        nav_type: index
      })
      this.data.list = []
      this.data.pageIndex = 1
      this.data.pageSize = 10
      this.getAlarmRecordList()
    }
  },

  //单选
  checkboxChange: function (e) {
    // console.log(e)
    var that = this
    var value = e.detail.value;
    var valLen = value.length
    var checkid = e.target.dataset.checkid
    var list = that.data.list
    var listLen = list.length
    var num = 0
    if(valLen != 0){      //选中
      for(var i=0;i<listLen;i++){
        if (list[i].AlarmRecordId == checkid) {//console.log(list[i].AlarmRecordId + 'if' +checkid)
          if (!list[i].checkeditem){
            list[i].checkeditem = true;
            num = num + 1;           //console.log('--' + num)
            that.data.trolleyArr.push(list[i].AlarmRecordId);
          }
        }else{
          if (list[i].checkeditem) {//console.log(list[i].AlarmRecordId + 'else' + checkid)
            num = num + 1;           //console.log('++' + num)
          }
        }
      }
      // console.log(that.data.trolleyArr)
      if(num == listLen){
        that.setData({
          checked_all:true  //全选
        })
      }
    }else{
      var arr = []
      var trolleyLen = that.data.trolleyArr.length
      for(var i=0;i<listLen;i++){
        if(list[i].AlarmRecordId == checkid){
          if(list[i].checkeditem){
            list[i].checkeditem = false
 
            for(var j=0;j<trolleyLen;j++){
              if(that.data.trolleyArr[j] == checkid){
                continue;
              }else{
                arr.push(that.data.trolleyArr[j])
              }
            }
          }
        }
      }
      that.setData({
        trolleyArr:arr,
        checked_all:false
      })
    }
      that.setData({
        list:list
      })
    // console.log(that.data.trolleyArr)
  },
  // 全选
  checkedAll:function(e) {
    var that = this
    var value = e.detail.value;
    var valLen = value.length
    var list = that.data.list
    var listLen = list.length
    // console.log(valLen)
    if (valLen != 0){
      for(var i=0;i<listLen;i++){
        list[i].checkeditem = true;
        that.data.trolleyArr.push(list[i].AlarmRecordId)
      }
      that.setData({
        checked_all:true,//全选
        list:list
      })
    }else{
      for(var i=0;i<listLen;i++){
        list[i].checkeditem = false
      }
      that.setData({
        trolleyArr:[],
        checked_all:false,
        list:list
      })
    } 
  },

  /**
   * 上拉加载
   */
  onReachBottom() {
    const that = this;
    if (!that.data.loading) {
      that.setData({
        loading: true, //加载中
        loaded: false //是否加载完所有数据
      });
    }
    that.data.pageIndex++
    this.getAlarmRecordList()
  },
  /**
   * 下拉刷新
   */
  onPullDownRefresh() {
    var that = this
    wx.showNavigationBarLoading() //在标题栏中显示加载圈圈
    that.data.pageIndex = 1
    this.data.list = []
    wx.hideNavigationBarLoading()
    this.getAlarmRecordList()
  },
  // 跳转详情页
  jumpDetails(e){
    var that = this
    //拿到点击的index下标
    var index = e.currentTarget.dataset.index
    //将对象转为string
    var item = JSON.stringify(that.data.list[index])
    wx.navigateTo({
      url: './details?info=' + item,
    })
  }
})