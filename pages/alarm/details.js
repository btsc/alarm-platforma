// pages/notice/details.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: null,
    DealRamark: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var item = JSON.parse(options.info);
    that.setData({
        info: item
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  userInput(e) {
    this.setData({
      'DealRamark' : e.detail.value,
    })
  },
  updatePopup() {
    var that = this
    var params = {
      ...that.data.info,
      "DealRamark": that.data.DealRamark,
      "DealFlagTypeId": 2
    }

    app.fetch.apis('api/Report/DealAlarmRecord', params,app.globalData.token).then(
      res => {
        if (res.State === 'success') {
          console.log('12')
        } else {
          wx.showModal({
            showCancel: false,
            content: res.Message
          });
        }
      }
    )
  }
})