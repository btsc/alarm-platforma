//获取应用实例
var app = getApp();

Page({
  data: {
    pageIndex: 1, // 当前第几页
    pageSize: 10, // 一页展示几条
    list: [],
    loading: false, // 是否展示 “正在加载” 字样
    loaded: false // 是否展示 “已加载全部” 字样
  },
  onShow:function (){
    var that = this
    that.data.list = []
    that.getDeviceTypeList()
  },
  onLoad: function (options) {

  },

  // 消息通知
  getDeviceTypeList() {
    var that = this
    var params = {
      "OrderBy": 'NoticeTime',
      "IsDesc": true,
      "Offset": (that.data.pageIndex - 1) * that.data.pageSize,
      "RowCount": that.data.pageSize,
    }
    app.fetch.apis('api/Report/GetMessageNoticeList', params ,app.globalData.token).then(
      res => {
        if (res.State === 'success') {
          if(res.Data.length === 0 || res.Data.length > that.data.pageSize) {
            that.data.pageIndex--
            that.setData({
              loading: false, //加载中
              loaded: true //是否加载完所有数据
            });
          } else {
            that.setData({
              list: that.data.list.concat(res.Data)
            })
            that.setData({
              loading: false, //加载中
              loaded: false //是否加载完所有数据
            });
          }
          wx.hideNavigationBarLoading()
          wx.stopPullDownRefresh()
        } else {
          wx.showModal({
            showCancel: false,
            content: res.Message
          });
        }
      }
    )
  },
  /**
   * 上拉加载
   */
  onReachBottom() {
    const that = this;
    if (!that.data.loading) {
      that.setData({
        loading: true, //加载中
        loaded: false //是否加载完所有数据
      });
    }
    that.data.pageIndex++
    this.getDeviceTypeList()
  },
  /**
   * 下拉刷新
   */
  onPullDownRefresh() {
    var that = this
    wx.showNavigationBarLoading() //在标题栏中显示加载圈圈
    that.data.pageIndex = 1
    this.data.list = []
    wx.hideNavigationBarLoading()
    this.getDeviceTypeList()
  },
  // 跳转详情页
  jumpDetails(e){
    var that = this
    //拿到点击的index下标
    var index = e.currentTarget.dataset.index
    //将对象转为string
    var item = JSON.stringify(that.data.list[index])

    if (that.data.list[index].IsRead !== 1) {
      var params = {
        ...that.data.list[index],
        IsRead: 1
      }
      app.fetch.apis('api/Report/DealMessageNotice', params ,app.globalData.token).then(
        res => {
          if (res.State === 'success') {
            return
          } else {
            wx.showModal({
              showCancel: false,
              content: res.Message
            });
          }
        }
      )
    }
    wx.navigateTo({
      url: './details?info=' + item,
    })
  }
})
