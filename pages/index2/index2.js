const app = getApp();
const deciveTypeArr = [
  {
    Text:'设备1',
    Value:'t1',
  },
  {
    Text:'设备2',
    Value:'t2',
  },
  {
    Text:'设备3',
    Value:'t3',
  },
  {
    Text:'设备4',
    Value:'t4',
  }
]
const manufacturerArr = [
  {
    Text:'厂家1',
    Value:'c1',
  },
  {
    Text:'厂家2',
    Value:'c2',
  }
]
Page({
  /**
   * 页面的初始数据
   */
  data: {
   
    list:  {
      deviceType: undefined,
      manufacturer: undefined,
    },
    selectnName:'',
    options1: [], //数据集
    options2: [], //数据集
    topsHeight: "60%", //高度
    opacity: "0.5", //透明度
    IsSingle: true //是否单选
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //初始化出库类型
    this.initExitType();
    
  },
  /**
   * 初始化出库类型
   */
  initExitType() {
    /* 用setTimeout 模拟接口*/
    setTimeout(() => {
      this.setData({
        options1: deciveTypeArr,
        options2: manufacturerArr
      })
    }, 1000)
  },
  /**
   * 下拉框选择完成事件
   */
  SelectFinish: function (even) {
    const selectName = this.data.selectnName;
    const obj = this.data.list
    obj[selectName] = even.detail
   console.log('obj', obj)
   
   this.setData({
     list: obj
   })
  },
  /**
   * 显示下拉框
   */
  selectStockOutType: function (e) {
    // 也就是id的name;
    const name = e.currentTarget.dataset.name
    console.log('name',  name )
    this.selectComponent("#" + name).showPopup();
    this.setData({
      selectnName:name
    })
  }
})
