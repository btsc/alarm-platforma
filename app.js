// app.js
const config = require('./config.js');
const fetch = require('./request.js');

App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
  },
  globalData: {
    userInfo: '1',
    token: null,
    serverUrl: config.serverUrl
  },
  fetch,
  setToken(key,token){
    //保存token到全局（中括号使这个key表示的变量名）
    this[key]=token

    //保存token到本地缓存
    wx.setStorageSync(key, token)
  }
})
