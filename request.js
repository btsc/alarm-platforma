const my_config = require('./config.js');
 
export const apis = (url, params, token, method = 'POST') => {
    let config = {};
    config['method'] = method;
    config['url'] = my_config.serverUrl + url;
    if (method == 'POST') {
        config['data'] = params;
    } else if (method == 'GET') {
        config['params'] = params;
    }
    return new Promise((resolve, reject) => {
        wx.request({
            ...config,
            header: {
                'Content-Type': 'application/json; charset=UTF-8',
                "Token": token
            },
            success(request) {
                resolve(request.data);
                wx.hideLoading();
            },
            fail() {
                wx.hideLoading();
                reject();
            }
        })
    })
}